This app packages Cantaloupe Server 

Cantaloupe is an open-source dynamic image server for on-demand generation of derivatives of high-resolution source images. With available operations including cropping, scaling, and rotation, it can support deep-zooming image viewers, as well as on-the-fly thumbnail generation. Compliance with the IIIF Image API enables it to work with a growing number of client applications ; see the Awesome IIIF page witch list many of this applications : images viewers, annotations tools, CMS and Digital Asset Management integration, funs, etc.
