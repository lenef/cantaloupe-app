#!/bin/bash

set -eu -o pipefail

CANTALOUPE_VERSION=5.0.6

mkdir -p /app/data/images
mkdir -p /app/data/cache
mkdir -p /app/data/logs

if [[ -f /sys/fs/cgroup/cgroup.controllers ]]; then # cgroup v2
    LIMIT=$(($(cat /sys/fs/cgroup/memory.max)/2**20))
else
    LIMIT=$(($(cat /sys/fs/cgroup/memory/memory.limit_in_bytes)/2**20))
fi
export JAVA_OPTS=" -Xms4g -Xmx${LIMIT}g -XX:MaxRAM=${LIMIT}m -XX:MaxRAMPercentage=75"

if [[ -f "/app/data/cantaloupe.properties" ]]; then
    CONFIG=/app/data/cantaloupe.properties 
else    
    CONFIG=/app/code/cantaloupe.properties 
fi

chown -R cloudron:cloudron /app/data

echo "==> Starting Cantaloupe"

exec gosu cloudron:cloudron java ${JAVA_OPTS} -Dcantaloupe.config=${CONFIG} -jar /app/code/cantaloupe-${CANTALOUPE_VERSION}.jar  
