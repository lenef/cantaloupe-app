# Cantaloupe app

Cantaloupe is a high-performance dynamic image server for on-demand generation of derivatives of high-resolution source images. Compliant with the IIIF Image API.