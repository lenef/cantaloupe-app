FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4 

RUN mkdir -p /app/code 
WORKDIR /app/code

ARG CANTALOUPE_VERSION=5.0.6

RUN apt-get update && \
        apt-get install -y openjdk-11-jre-headless \ 
            ffmpeg \
            libopenjp2-tools \
            liblcms2-dev \
            libpng-dev \
            libzstd-dev \
            libtiff-dev \
            libjpeg-dev \
            zlib1g-dev \
            libwebp-dev \
            libimage-exiftool-perl \
            imagemagick \
            libturbojpeg libturbojpeg0-dev 
            
RUN rm -rf /var/cache/apt /var/lib/apt/lists

# Install Cantaloupe

RUN wget https://github.com/cantaloupe-project/cantaloupe/releases/download/v${CANTALOUPE_VERSION}/cantaloupe-${CANTALOUPE_VERSION}.zip -O /tmp/cantaloupe.zip  

RUN unzip /tmp/cantaloupe.zip -d /tmp 
RUN rm -Rf /app/code/*
RUN cp -Rp /tmp/cantaloupe-${CANTALOUPE_VERSION}/* /app/code/
RUN rm -Rf /tmp/cantaloupe* 

RUN cp /app/code/cantaloupe.properties.sample /app/code/cantaloupe.properties
RUN sed -e ' \
s,FilesystemSource.BasicLookupStrategy.path_prefix = /home/myself/images/,FilesystemSource.BasicLookupStrategy.path_prefix = /app/data/images/, ; \
s,/path/to/,/app/data/, ; \
s,cache.server.derivative.enabled = false,cache.server.derivative.enabled = true, ; \
s,cache.server.derivative =,cache.server.derivative = FilesystemCache, ; \
s,cache.server.purge_missing = false,cache.server.purge_missing = true, ; \
s,cache.server.resolve_first = false,cache.server.resolve_first = true, ; \
s,cache.server.worker.enabled = false,cache.server.worker.enabled = true, ; \
s,FilesystemCache.pathname = /var/cache/cantaloupe,FilesystemCache.pathname = /app/data/cache, ;' -i /app/code/cantaloupe.properties

RUN chown -R cloudron:cloudron /app/code 

# add code

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
